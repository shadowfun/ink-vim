if !exists("g:ink_bullet_indentation")
  let g:ink_bullet_indentation = 1  " Should bullet lines be indented?
endif

let s:shiftwidth = 2

setlocal expandtab
let &l:shiftwidth=s:shiftwidth  " 'let &l:' is equivalent to 'setlocal'
setlocal softtabstop=-1  " -1 == use same value as shiftwidth
setlocal breakindent
setlocal showbreak=<>\ 

setlocal indentkeys=<Space>,!^F,o,O
setlocal indentexpr=GetInkIndent()
function! GetInkIndent()
    let lnum = line('.')
    let [bullets, start] = s:CountBullets(getline(lnum))
    if bullets > 0 " Indent is determined by the line itself.
        return g:ink_bullet_indentation ? s:CalcIndent(bullets) : 0
    else
        " Indent is determined by the previous line.
        let [bullets, start] = s:CountBullets(getline(lnum - 1))
        if bullets > 0
            " Previous line has a bullet; we need to account for that.
            return len(start)
        else
            " No bullet, just copy previous indent
            return indent(lnum - 1)
        endif
    endif
endfunction

function! s:CalcIndent(symbols)
    " If there are m symbols, we need to indent n = n-1 times
    " (the first symbol is flush left, the second is indented once, etc).
    " Each indent is the sum of all previous indents, i.e. a triangle number.
    " (Triangle numbers are n+(n-1)+(n-2)+…+1 = (n²+n)/2.)
    let n = a:symbols - 1
    let triang = ((n*n)+n)/2
    return max([triang * &l:shiftwidth, 0])
endfunction

function! s:CountBullets(line)
    " Return the number of bullets at the start of line.

    " The pattern needs to look this funky so we don’t catch a divert
    " as a bullet for a gather (i.e. - - -> target)
    let pat = '\v^(\s*%(\*\s*|\+\s*|\-\s*)+ ).+$'
    if match(a:line, pat) >= 0
        let start = substitute(a:line, pat, '\1', "")
        let symbols = substitute(start, '\s', '', 'g')  " remove all spaces
        return [len(symbols), start]
    else
        return [0, ""]
    endif
endfunction
