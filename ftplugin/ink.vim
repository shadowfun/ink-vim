setlocal commentstring=//\ %s

" Folding
setlocal foldmethod=expr
setlocal foldexpr=GetInkFold(v:lnum)

function! GetInkFold(lnum)
    let line = getline(a:lnum)

    if line =~ "^\=\="
        return '>1'
    elseif line =~ "^\= "
        return '>2'
    else
        return '='
    endif
endfunction

" Abbreviations
iab 2* *<Tab>*
iab 3* *<Tab>*<Tab>*
iab 4* *<Tab>*<Tab>*<Tab>*
iab 5* *<Tab>*<Tab>*<Tab>*<Tab>*
iab 6* *<Tab>*<Tab>*<Tab>*<Tab>*<Tab>*
iab 7* *<Tab>*<Tab>*<Tab>*<Tab>*<Tab>*<Tab>*
iab 8* *<Tab>*<Tab>*<Tab>*<Tab>*<Tab>*<Tab>*<Tab>*
iab 9* *<Tab>*<Tab>*<Tab>*<Tab>*<Tab>*<Tab>*<Tab>*<Tab>*
iab 2+ +<Tab>+
iab 3+ +<Tab>+<Tab>+
iab 4+ +<Tab>+<Tab>+<Tab>+
iab 5+ +<Tab>+<Tab>+<Tab>+<Tab>+
iab 6+ +<Tab>+<Tab>+<Tab>+<Tab>+<Tab>+
iab 7+ +<Tab>+<Tab>+<Tab>+<Tab>+<Tab>+<Tab>+
iab 8+ +<Tab>+<Tab>+<Tab>+<Tab>+<Tab>+<Tab>+<Tab>+
iab 9+ +<Tab>+<Tab>+<Tab>+<Tab>+<Tab>+<Tab>+<Tab>+<Tab>+
iab 2- -<Tab>-
iab 3- -<Tab>-<Tab>-
iab 4- -<Tab>-<Tab>-<Tab>-
iab 5- -<Tab>-<Tab>-<Tab>-<Tab>-
iab 6- -<Tab>-<Tab>-<Tab>-<Tab>-<Tab>-
iab 7- -<Tab>-<Tab>-<Tab>-<Tab>-<Tab>-<Tab>-
iab 8- -<Tab>-<Tab>-<Tab>-<Tab>-<Tab>-<Tab>-<Tab>-
iab 9- -<Tab>-<Tab>-<Tab>-<Tab>-<Tab>-<Tab>-<Tab>-<Tab>-

" Errors / Compiler
set errorformat=%EError:\ '%f'\ line\ %l:\ %m
set errorformat+=%WWarning:\ '%f'\ line\ %l:\ %m
"set errorformat+=%ITODO:\ '%f'\ line\ %l:\ %m
