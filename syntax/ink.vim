" Generic (Applicable Everywhere) {{{
syntax region inkComment oneline excludenl start="//" end="$"
syntax region inkComment start="/\*" end="\*/"
highlight link inkComment Comment

syntax keyword inkTodos TODO XXX FIXME NOTE
highlight default link inkTodos Todo

syntax match inkKeyword /\v^%(VAR|CONST|LIST)/
highlight default link inkKeyword Keyword

syntax match inkInclude /\v^INCLUDE/
highlight default link inkInclude Include

syntax region inkTag oneline excludenl matchgroup=Delimiter start="#" end="\ze\v#|$"
highlight default link inkTag Comment

syntax match inkDivert /\%(->\|<-\)\%( \i\+\)\?/ containedin=ALL
highlight default link inkDivert Label

syntax match inkGlue "<>" containedin=ALL
highlight default link inkGlue Operator
" (End Generic) }}}

" Sectioning {{{
syntax region inkKnot oneline keepend matchgroup=Delimiter
            \ start="\v^\=\=+\s+" end="\v%(\s+\=+\s*)?$"
syntax region inkStitch oneline keepend matchgroup=Delimiter
            \ start="\v^\=\s+" end="\v%(\s+\=\s*)?$"
highlight default link inkKnot Label
highlight default link inkStitch Label

" Functions {{{
syntax keyword inkKeyword function contained containedin=inkKnot nextgroup=inkFunction
syntax match inkFunction /\v\i+\ze\(/ contained containedin=inkKnot,inkLogic nextgroup=inkFuncArgs
highlight default link inkFunction Function
syntax region inkFuncArgs matchgroup=Delimiter start="(" end=")" contained
" (End Functions) }}}
" (End Sectioning) }}}

" Choices, Gathers {{{
syntax match inkChoice "\v^\s*%(\*\s*|\+\s*|\-%(\>)@!\s*)+.+$"

syntax match inkChoiceBulletStart  "\v\*" contained containedin=inkChoice nextgroup=inkChoiceBulletStart,inkChoiceBullet skipwhite
syntax match inkChoiceBullet  "\v\*\s+\*@!" contained containedin=inkChoice nextgroup=inkLabel,inkLogic skipwhite
syntax match inkChoiceBulletStart  "\v\+" contained containedin=inkChoice nextgroup=inkChoiceBulletStart,inkChoiceBullet skipwhite
syntax match inkChoiceBullet  "\v\+\s+\+@!" contained containedin=inkChoice nextgroup=inkLabel,inkLogic skipwhite
syntax match inkChoiceBulletStart  "\v\-" contained containedin=inkChoice nextgroup=inkChoiceBulletStart,inkChoiceBullet skipwhite
syntax match inkChoiceBullet  "\v\-\s+\-@!" contained containedin=inkChoice nextgroup=inkLabel,inkLogic skipwhite

highlight default link inkChoiceBullet Type

if g:ink_bullet_indentation
    highlight default link inkChoiceBulletStart inkChoiceBullet
else
    highlight default link inkChoiceBulletStart SpecialKey
endif

syntax match inkLabel /(\i\+)/ contained nextgroup=inkLogic skipwhite
highlight default link inkLabel Label

syntax region inkAltText oneline keepend contained containedin=inkChoice matchgroup=Delimiter
            \ start="\[" end="\]"
highlight default link inkAltText Comment
" (End Choices) }}}

" Logic {{{
syntax region inkLogic matchgroup=Delimiter start="{" end="}"
syntax region inkLogic matchgroup=Delimiter oneline keepend start="\v\s*\~" end="$" containedin=ALL

syntax match inkLogicDelims contained containedin=inkLogic "|\|\:"
highlight default link inkLogicDelims Delimiter

syntax match inkOperator contained containedin=inkLogic
            \ /==\|!=\|not\|?\|!?\|&&\|||\|+=\|-=\|>=\|<=\|>\|<\|=/
highlight default link inkOperator Operator
" (End Logic) }}}
